from multiprocessing import Pool
import requests,re,ray,multiprocessing
from bs4 import BeautifulSoup 
ray.init()
reviews = open("reviews_10.txt","w+")

def bs_parse(url):
    global reviews
    r = requests.get(url)
    soup = BeautifulSoup(r.content, 'html5lib')
    z = []
    title = re.findall("^<title>(.+)</title>$",str(soup.find('title')))[0]
    type = soup.find("meta",  property="og:type")
    keywords = soup.findAll(attrs={"name": re.compile(r"keywords", re.I)})
    description = soup.findAll(attrs={"name": re.compile(r"description", re.I)})
    keywords = keywords[0]['content'].encode('utf-8')
    description = description[0]['content'].encode('utf-8')
    keywords = ''.join(keywords) if isinstance(keywords, list) else str(keywords)
    description = str(''.join(description)) if isinstance(description, list) else str(description)
    title = ''.join(title) if isinstance(title, list) else str(title)
    title = title.split("|")[0]
    type = ''.join(type).strip() if isinstance(type, list) else str(title).strip()

    language = re.findall("marathi|hindi|kanada|telugu|punjabi|gujrati|bengali", description, re.IGNORECASE)
    language = "".join(map(str.lower, language))
    location = re.findall("bengaluru|pune|mumbai|chennai|chandigarh|ahmedabad|hyderabad|ncr", description, re.IGNORECASE)
    location = ",".join(map(str.lower, location))

    title = title.split(":")
    username = title[0].strip()
    if not username.strip():
        return 0
    title = "".join(title[1:]).lstrip()
    dic = {"username": username, "title": title, "type": type, "description": description,
            "keywords": keywords, "language": language, "location": location}

    template = "username:{username}~title:{title}~keywords:{keywords}~description:{description}~language:{language}~{location}:location"

    reviews.write(template.format(**dic) + "\n")
    return 1
pool = Pool()
for i in range(72573750,72580000):
    url = "https://in.bookmyshow.com/reviews/popular/"+str(i)
    #result = pool.apply_async(bs_parse, url)
    bs_parse(url)
    print i
