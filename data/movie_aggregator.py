import csv,re,numpy,sys
from collections import defaultdict
argv = sys.argv

movies=open("movies_merged_parsed.txt")
movies = csv.reader(movies,delimiter='~')
mov_writer = csv.writer(open("movies_merged_aggr.txt","w+"),delimiter="~")
mov_headers = next(movies, None)
mov_header = {mov_headers[i]:i for i in range(len(mov_headers))}
mov_names = set()
cs = ['action','drama','adventure','romantic','animation','comedy','musical','thriller','biography','fatasy','horror','mystery']
lns = ['Telugu', 'Marathi', 'Punjabi', 'Hindi', 'Tamil', 'Bengali']
header = ['name']+lns+cs
mov_writer.writerow(header)
for movie in movies:
    category = re.findall(
        "action|drama|adventure|romantic|animation|comedy|musical|thriller|biography|fatasy|horror|mystery",
        movie[mov_header["description"]], re.IGNORECASE)
    if not movie[mov_header['name']].strip() in mov_names:
        categories = map(lambda x: 1 if x in category else 0,cs)
        languages = map(lambda x: 1 if movie[mov_header['language']]==x else 0, lns)
        mov_writer.writerow([movie[mov_header['name']].strip()]+languages+categories)
    mov_names.add(movie[mov_header['name']].strip())


