import csv,re,numpy,sys

argv=sys.argv

if len(argv)<2:
    print "Please provide arguments : [input_file]"

input_file = argv[1]
output_file = argv[1].split(".")[0] + "_parsed.txt"
events=open(input_file,"r")
events = csv.reader(events,delimiter='~')
writer = csv.writer(open(output_file,"w+"),delimiter="~")
header,hflag = [],0

for event in events:
    event = map(lambda x : x.split(":"),event)
    for i in event:
        if not hflag:
            header+=[i[0]]
    if not hflag:
        writer.writerow(header)
        hflag = 1
    writer.writerow(map(lambda x:x[1],event))

