#!/usr/bin/python

import sys
from bookmyshow.spiders.BookMyShow import *

if __name__ == "__main__":
    if len(sys.argv)<1:
        print "Please Enter Url as Argument"
    sitemap = SiteMap().call(sys.argv[1])
