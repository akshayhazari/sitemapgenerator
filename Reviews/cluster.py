import nmslib,json,csv
from collections import defaultdict,Counter
import numpy,pandas,gc

ln = ['Telugu', 'Marathi', 'Punjabi', 'Hindi', 'Tamil', 'Bengali']
names = ['username','count']+ln+['language','gender','hasbooked']
names += ['action','drama','adventure','romantic','animation','comedy','musical','thriller','biography','fatasy','horror','mystery']
data = pandas.read_csv('reviews_merged_parsed_aggr.txt',sep='~',skiprows=[0],
                         usecols=[ i for i in range(0,8)] +[ i for i in range(9,len(names))] ,
                         names=names[0:8] +names[9:],
                         na_filter=False)


index = nmslib.init(method='hnsw', space='cosinesimil')
index.addDataPointBatch(data[names[1:8] +names[9:]])
index.createIndex({'post': 2})

# query for the nearest neighbours of the first datapoint


gc.collect()
ids, distances = index.knnQuery(data[names[1:8] +names[9:]], k=20)

# get all nearest neighbours for all the datapoint
# using a pool of 4 threads to compute
neighbours = index.knnQueryBatch(data[names[1:8] +names[9:]], k=20, num_threads=1)





# mov_cols = ['name','action','drama','adventure','romantic','animation','comedy','musical','thriller','biography','fatasy','horror','mystery']
# mov_cols += ['Telugu', 'Marathi', 'Punjabi', 'Hindi', 'Tamil', 'Bengali']
# mov_data = pandas.read_table('movies_merged_aggr.txt',sep='~',skiprows=[0],
#                          usecols=[ i for i in range(0,len(mov_cols))] ,
#                          names=mov_cols[0:] ,
#                          na_filter=False)

# index = nmslib.init(method='hnsw', space='cosinesimil')
# index.addDataPointBatch(mov_data[mov_cols[1:]])
# index.createIndex({'post': 2}, print_progress=True)

def get_random(n,p=[0.7,0.3]):
    return str(numpy.random.choice(numpy.arange(0,n),p=p))

# query for the nearest neighbours of the first datapoint


gc.collect()
#ids, distances = index.knnQuery(mov_data[mov_cols[1:]], k=5)
#
# # get all nearest neighbours for all the datapoint
# # using a pool of 4 threads to compute
# mov_neighbours = index.knnQueryBatch(mov_data[mov_cols[1:]], k=25, num_threads=1)

user_mov =csv.reader(open("user_movies.txt"),delimiter="~")
user_mov_d = defaultdict(list)
user_hasbooked = defaultdict(int)
for user in user_mov:
    user_hasbooked[user[0]]=user[1]
    user_mov_d[user[0]]=user[2:]


neighbour_mov_cnt= defaultdict()
cnt,ncnt = 0,0
for i in range(len(neighbours)):
    users = map(lambda x : data.iloc[x]['username'],neighbours[i][0])
    c = Counter(reduce(lambda x, y: x + y, map(lambda x: user_mov_d[x], users)))
    neighbour_mov_cnt[data.iloc[i]['username']] = dict(c)
    for user in users:
        if not user in neighbour_mov_cnt:
           neighbour_mov_cnt[user] = dict(c)
           cnt+=1
        else:
           ncnt +=1
print len(neighbour_mov_cnt.keys())

# print neighbours
# Get ads
headers,header = [],{}

advertisement = defaultdict(list)

with open('advertisement.txt') as ad:
    ad_reader = csv.reader(ad, delimiter="\t")
    headers = next(ad_reader, None)
    header = {headers[i]: i for i in range(len(headers))}
    for adv in ad_reader:
        if adv[header["type"]] == 'adventure':
            advertisement[adv[header["type"]]]+=[adv]
        elif adv[header["gender"]] in ('f','m'):
            advertisement[adv[header["gender"]]] += [adv]
        elif adv[header["language"]] not in ['a']:
            advertisement[adv[header["language"]]] += [adv]
        elif  adv[header["type"]] == 'bank' :
            advertisement[adv[header["type"]]] += [adv]

rec_writer = csv.writer(open("user_adv_recommendations.txt","w+"),delimiter="\t")
recm_writer = csv.writer(open("user_mov_recommendations.txt","w+"),delimiter="\t")
recommend = defaultdict(list)
rec_writer.writerow(["username"]+headers)
for index, row in data.iterrows():
    if int(user_hasbooked[row['username']]) :
        movies = []
        if row['username']  in neighbour_mov_cnt:
            movies = sorted([(k, neighbour_mov_cnt[row['username']][k]) for k in neighbour_mov_cnt[row['username']]], key=lambda x: -x[1])
        recommend[row['username']] = movies
        recm_writer.writerow([row["username"]]+movies)
    else:
        l = ['bengali','marathi','tamil','telugu','punjabi']
        lang = sorted([(i,row[i]) for i in map(str.title,l)],key = lambda x : -x[1])
        toplang = lang[0][0] if lang[0][1] else 0
        gen_r = advertisement[row['gender']]
        p_l,p_a,p_g,p_b = [],[],[],[]
        p = []
        lang_r =[]
        adv_r = []
        if toplang:
            lang_r=advertisement[toplang.lower()]
            p_l = [0.4]
        if row['adventure']:
            adv_r = advertisement['adventure']
            p_a = [0.2]*3
        if p_l and p_a:
            p = [0.05,0.2,0.2,0.2,0.3,0.025,0.025]
        elif p_l:
            p = p_l+[0.35,0.125,0.125]
        elif p_a:
            p = p_a + [0.3, 0.05,0.05]
        else:
            p = [0.6, 0.2,0.2]
        if not gen_r:
            g = p.pop(0)
            inc=g/float(len(p))
            p= map(lambda x : x+inc,p)
        ban_r = advertisement['bank']
        rec = gen_r+lang_r + adv_r+ban_r
        print p,rec
        p = get_random(len(rec),p=p)

        rec_writer.writerow([row["username"]]+rec[int(p)])