import csv,re,numpy,sys
from collections import defaultdict
argv = sys.argv

if len(argv)<2:
    print "Please provide input file"

reviews=open(argv[1])
reviews = csv.reader(reviews,delimiter='~')
writer = csv.writer(open(argv[1].split(".")[0]+"_aggr.txt","w+"),delimiter="~")
header,hflag = [],0

d = defaultdict()

headers = next(reviews, None)
header = {headers[i]:i for i in range(len(headers))}

movies=open("movies_merged_parsed.txt")
movies = csv.reader(movies,delimiter='~')
cat_dic = defaultdict(list)
mov_headers = next(movies, None)
mov_header = {mov_headers[i]:i for i in range(len(mov_headers))}
for movie in movies:
    category = re.findall(
        "action|drama|adventure|romantic|animation|comedy|musical|thriller|biography|fatasy|horror|mystery",
        movie[mov_header["description"]], re.IGNORECASE)
    cat_dic[movie[mov_header['name']]] = category

lc = set()
ln = defaultdict(int)

user_movie=defaultdict(list)
user_hasbooked = defaultdict(int)
for review in reviews:
    title = review[1].split("Movie")[0]
    title = title.split("(")[0]
    name = title.strip().lower()
    name = '-'.join(name.split())
    cat = []
    user_movie[review[0]]+=[name]

    if name in cat_dic:
        cat = cat_dic[name]
    if review[0] not in d:
        d[review[0]] = defaultdict(int)
        d[review[0]]["count"] += 1
        d[review[0]]["location"] = defaultdict(int)
        user_hasbooked[review[0]]=d[review[0]]["hasbooked"]= review[header["hasbooked"]]
        if review[header["location"]]:
            d[review[0]]["location"][review[header["location"]]]+=1
        d[review[0]]["language"] = defaultdict(int)
        if review[header["language"]]:
            d[review[0]]["language"][review[header["language"]]]+=1
        lc.add(review[header["location"]])
        d[review[0]]["category"]= defaultdict(int)
        ln[review[header["language"]]]+=1
        d[review[0]]["locations"] = [review[header["location"]]]
        d[review[0]]["languages"] = [review[header["language"]]]
        d[review[0]]["gender"] = review[header["gender"]]
    else:
        language = review[header["language"]]
        location = review[header["location"]]
        location_1 = re.findall("bengaluru|pune|mumbai|chennai|chandigarh|ahmedabad|hyderabad|ncr", review[header["description"]],
                              re.IGNORECASE)
        location_1 = filter(None,location_1)
        location_1 = location_1[0] if location_1 else ""
        lc.add(location)
        lc.add(location_1)
        ln[language]+=1
        hasbooked = review[header["hasbooked"]]

        d[review[0]]["count"]+=1
        d[review[0]]["hasbooked"] = hasbooked

        if location:
            d[review[0]]["location"][location]+=1
            d[review[0]]["locations"] += [location]
        if language:
            d[review[0]]["language"][language]+=1
            d[review[0]]["languages"] += [language]
    for i in cat:
        d[review[0]]["category"][i] += 1

w = csv.writer(open("user_movies.txt","w+"),delimiter="~")
for i in user_movie:
    w.writerow([i]+[user_hasbooked[i]]+list(set(user_movie[i])))

del ln['']
lns = ['Telugu', 'Marathi', 'Punjabi', 'Hindi', 'Tamil', 'Bengali']
cs = ['action','drama','adventure','romantic','animation','comedy','musical','thriller','biography','fatasy','horror','mystery']
header = ['username','count']+lns+['language','gender','hasbooked']+cs
writer.writerow(header)
for i in d:
    gender = {'m':1,'f':0}
    languages = map(lambda x: 0 if x == None else x, [(d[i]["language"].get(j)) for j in lns])
    categories = map(lambda x: 0 if x == None else x, [(d[i]["category"].get(j)) for j in cs])
    row = [i,d[i]["count"],"-".join(sorted(list(set(filter(None,d[i]['languages']))))),gender[d[i]["gender"]],d[i]["hasbooked"]]
    for k in languages:
        row.insert(2,k)
    row+=categories
    writer.writerow(row)



