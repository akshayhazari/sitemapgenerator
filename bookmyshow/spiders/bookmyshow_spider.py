import scrapy
#from scrapy.contrib.linkextractors.htmlparser import HtmlLinkExtractor
from scrapy.linkextractors.lxmlhtml import LxmlLinkExtractor
from scrapy.linkextractors.sgml import  SgmlLinkExtractor
from scrapy.spiders import Rule, CrawlSpider
from scrapy.linkextractors import LinkExtractor
import urllib2,requests
from scrapy.utils.project import get_project_settings
settings = get_project_settings()
#from scrapy.conf import settings
from requests.auth import HTTPBasicAuth
import urllib2,requests,os,sys,re
from urlparse import urlparse
from bookmyshow.items import BookMyShowItem
import time,datetime,warnings
warnings.filterwarnings('ignore')


class BookMyShow(CrawlSpider):
    name = "bookmyshow"
    
    def __init__(self,domain='',*args,**kwargs):
        
        super(BookMyShow, self).__init__(*args, **kwargs)
        start_url = kwargs.get('start_url')
        global start_urls,rules 
        settings.set('DEPTH_LIMIT',1)
        settings.set('ROBOTSTXT_OBEY',True)
        self.path = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
        self.movies = self.init_file("movies")
        self.movies = open(self.movies,'a+')
        self.sports = self.init_file("sports")
        self.sports = open(self.sports,'a+')
        self.activities = self.init_file("activities")
        self.activities = open(self.activities,'a+')
        self.plays = self.init_file("plays")
        self.plays = open(self.plays,'a+')
        self.events = self.init_file("events")
        self.events = open(self.events,'a+')
        self.reviews = self.init_file("reviews")
        self.reviews = open(self.reviews,'a+')
        self.advertisement = self.init_file("advertisement")
        self.advertisement = open(self.advertisement,'a+')
        self.start_urls = [start_url]
        start_urls = self.start_url
        self.first = 0
        self.rules = (Rule(LxmlLinkExtractor(allow_domains = [self.extract_domain(start_url)]), callback=self.parse_items, follow=True),)
        rules = self.rules
        self._rules = rules


    def init_file(self,type):

        if not os.path.exists(os.path.join(self.path,"data")):
            os.mkdir(os.path.join(self.path,"data"))

        filename = self.get_filename(type)

        fd = open(filename,'w+')
        #fd.write('<?xml version="1.0" encoding="UTF-8"?>\n<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">\n')

        return filename
        
    
    def extract_domain(self,url):
        return urlparse(url).netloc

    
    def get_filename(self,type):
        op_path=os.path.join(self.path,"data/"+type+".txt")
        if os.path.exists(op_path):
            os.remove(op_path)
        return op_path

    def write_url(self,dic,type):
        if type =="movies" or type =="activities" or type =="events" or type =="plays" or type =="sports":
            template = "name:{name}~title:{title}~keywords:{keywords}~description:{description}~language:{language}~location:{location}"
            if type == "movies":
                self.movies.write(template.format(**dic)+"\n")
            elif type == "activities":
                self.activities.write(template.format(**dic) + "\n")
            elif type == "events":
                self.events.write(template.format(**dic) + "\n")
            elif type == "plays":
                self.plays.write(template.format(**dic) + "\n")
            elif type == "sports":
                self.sports.write(template.format(**dic) + "\n")
        elif type =="reviews":
            template = "username:{username}~title:{title}~keywords:{keywords}~description:{description}~language:{language}~location:{location}"
            self.reviews.write(template.format(**dic)+"\n")

    def last_modified(self,url):
        date = None
        try:
            conn = urllib2.urlopen(url, timeout=1)
            date = conn.headers['date']
            last_modified = conn.headers['last-modified']
            return last_modified
        except:
            return date if date else "NA"


    def parse_items(self, response):
        internal = LinkExtractor(allow_domains=[self.extract_domain(response.url.strip())])
        links = internal.extract_links(response)
        internal = []
        for link in links:
            internal.append(link.url)
            if "movies" in link.url:
                internal.append(link.url+"/user-reviews")
        
            
        for link in internal:
            yield scrapy.Request(link.strip(), callback=self.parse_attr,dont_filter=False)


    def parse_attr(self, response):
        bookmyshowitem = BookMyShowItem()
        url = response.url
        type = response.xpath("//meta[@property='og:type']/@content").extract()
        keywords = response.xpath("//meta[@name='keywords']/@content").extract()
        description = response.xpath("//meta[@name='description']/@content").extract()
        title = bookmyshowitem["title"] = response.xpath("//title/text()").extract()

        keywords = ''.join(keywords) if isinstance(keywords, list) else str(keywords)
        description = str(''.join(description)) if isinstance(description, list) else str(description)
        title = ''.join(title) if isinstance(title,list) else str(title)
        title = title.split("|")[0]
        type = ''.join(type).strip() if isinstance(type, list) else str(title).strip()
        language = re.findall("marathi|hindi|kanada|telugu|punjabi|gujrati|bengali", description, re.IGNORECASE)
        #language = "".join(map(str.lower, language))
        language = language[0].lower() if language else ""
        location = re.findall("bengaluru|pune|mumbai|chennai|chandigarh|ahmedabad|hyderabad|ncr", description, re.IGNORECASE)
        location = location[0].lower() if location else ""
        type = "reviews" if "/reviews/"  in url else "movies" if "/movies/" in url else "events" if "/events/" in url \
            else "activities" if "/activities/" in url  else  "sports" if "/sports/" in url else "plays" if "/plays/" else type
        if "user-reviews" in url :
            return bookmyshowitem
        if type == "movies" or type == "events" or type =="activities" or type == "sports" or type == "plays":
            url_dict = {"title": title,"type":type,"description":description,"keywords":keywords,
                        "language":language,"location":location}
            name = re.findall(type+"/([^/]+)/",url)
            name = name[0] if name else title.lower()
            url_dict.update({"name":name})
            self.write_url(url_dict,type)
        elif type=="reviews":
            title = title.split(":")
            username = title[0].strip()
            title = "".join(title[1:]).lstrip()
            url_dict = {"username":username,"title": title, "type": type, "description": description,
                        "keywords": keywords,"language":language,"location":location}
            self.write_url(url_dict, "reviews")
        elif type == "advertisement":
            pass
        return bookmyshowitem
